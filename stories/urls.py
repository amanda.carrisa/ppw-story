from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('story1',story1, name='story1'),
    path('story3',story3, name='story3'),
    path('story32',story32, name='story32'),

]
